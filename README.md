# Micropub

A command-line tool to interact with [Micropub](https://micropub.spec.indieweb.org/) servers.

## Building

Building the tool can be performed by running:

```sh
go build
```

This will produce a command-line application `micropub`.

Pre-built releases can be found [on GitLab](https://gitlab.com/jamietanna/micropub-go/-/releases).

## Configuration

By default, a `default` profile is required, which provides configuration for the given identity to be used for a given Micropub server.

If you have multiple identities, or Micropub servers, creating new profiles will allow easier switching between them, using the `-p` / `--profile` flag.

Configuration can be bootstrapped using the `micropub config init` command, alternatively you can manually create the files:

### Configuration Files

A configuration file can be used, expected to be in the location `$HOME/.config/micropub/config.toml`, but can be configured using the `-c` / `--config` flag on the command-line.

 with the following configuration:

```toml
[profiles]
[profiles.default]
me = "..."
```

Authentication can be set up / refreshed by running `micropub auth reauth`.

### Environment Variables

This tool uses [Viper](https://github.com/spf13/viper), which means that you can use environment variables, too, like so:

```sh
# notice that the property name is capitalised!
env PROFILES.DEFAULT.ENDPOINT=https://www-api.jvt.me/micropub/media \
  ./micropub ...
```

### Discovering configuration

Once the `me` is specified, running `micropub config discover` will look up a `rel=micropub` in the `me` and update the configuration accordingly, discovering other metadata through `q=config` accordingly.

## Stability

This is still a _very_ early version, and as such I'd recommend even myself as the author avoid using it too much.
