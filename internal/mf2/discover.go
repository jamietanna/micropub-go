package mf2

import (
	"fmt"
	"net/http"
	"net/url"

	"willnorris.com/go/microformats"
)

// DiscoverMicropubEndpoint Discover the micropub endpoint for a given profile URL
func DiscoverMicropubEndpoint(me string) (string, error) {
	meURL, err := url.Parse(me)
	if err != nil {
		return "", err
	}
	resp, err := http.Get(me)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	data := microformats.Parse(resp.Body, meURL)
	if _, ok := data.Rels["micropub"]; !ok {
		return "", fmt.Errorf("no micropub endpoint could be found at me=%s", meURL.String())
	}

	return data.Rels["micropub"][0], nil
}
