package mf2

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDiscoverMicropubEndpoint_ReturnsErrorWhenInvalidUrl(t *testing.T) {
	_, err := DiscoverMicropubEndpoint("")

	assert.NotNil(t, err)
}

func TestDiscoverMicropubEndpoint_ReturnsErrorWhenInvalidRequest(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
	}))
	defer srv.Close()

	_, err := DiscoverMicropubEndpoint(srv.URL)

	assert.NotNil(t, err)
}

func TestDiscoverMicropubEndpoint_ReturnsErrorWhenNoMicropubRel(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := DiscoverMicropubEndpoint(srv.URL)

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "no micropub endpoint could be found at me=%s", srv.URL)
	}
}

func TestDiscoverMicropubEndpoint_ReturnsMicropubWhenPresent(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		_, _ = w.Write([]byte(`
		<html>
			<head>
				<link rel="micropub" href="https://foo">
			</head>
		</html>
		`))
	}))
	defer srv.Close()

	micropub, err := DiscoverMicropubEndpoint(srv.URL)

	assert.Nil(t, err)
	assert.Equal(t, "https://foo", micropub)
}

func TestDiscoverMicropubEndpoint_ReturnsFirstWhenManyPresent(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		_, _ = w.Write([]byte(`
		<html>
			<head>
				<link rel="micropub" href="https://foo">
				<link rel="micropub" href="https://bar">
				<link rel="micropub" href="https://baz">
			</head>
		</html>
		`))
	}))
	defer srv.Close()

	micropub, err := DiscoverMicropubEndpoint(srv.URL)

	assert.Nil(t, err)
	assert.Equal(t, "https://foo", micropub)
}
