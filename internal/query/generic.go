package query

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

// GenericQuery Perform a given query to the Micropub server
func GenericQuery(accessToken string, endpoint string, args []string) (map[string]any, error) {
	rBody, err := GenericQueryRaw(accessToken, endpoint, args)
	if err != nil {
		return nil, err
	}

	var config map[string]any
	err = json.Unmarshal(rBody, &config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

// GenericQueryRaw Perform a given query to the Micropub server, returning the raw bytes from the request
func GenericQueryRaw(accessToken string, endpoint string, args []string) ([]byte, error) {
	client := &http.Client{}

	u, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}
	q := u.Query()
	q.Set("q", args[0])

	for _, arg := range args[1:] {
		parts := strings.Split(arg, "=")
		q.Add(parts[0], parts[1])
	}

	u.RawQuery = q.Encode()

	req, err := http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return nil, err
	}
	if len(accessToken) > 0 {
		req.Header.Add("Authorization", "Bearer "+accessToken)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.New("there was an error parsing the Micropub endpoint's config")
	}

	defer resp.Body.Close()
	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, but there was an issue parsing the body", endpoint, resp.StatusCode)
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, with body: \n%s", u.String(), resp.StatusCode, string([]byte(rBody)))
	}

	return rBody, nil
}
