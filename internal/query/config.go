package query

import (
	"encoding/json"
)

// MicropubConfig The response from the q=config query
type MicropubConfig struct {
	MediaEndpoint string `json:"media-endpoint,omitempty"`
}

// Config Perform the q=config query for the Micropub server
func Config(accessToken string, endpoint string) (*MicropubConfig, error) {
	rBody, err := GenericQueryRaw(accessToken, endpoint, []string{"config"})
	if err != nil {
		return nil, err
	}

	config := MicropubConfig{}
	err = json.Unmarshal(rBody, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
