package query

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSource_SendsGET(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "GET", r.Method)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("", srv.URL, "")

	assert.Nil(t, err)
}
func TestSource_SetsQuery(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "q=source&url=foo", r.URL.RawQuery)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("", srv.URL, "foo")

	assert.Nil(t, err)
}
func TestSource_SetsAcceptHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "application/json", r.Header.Get("accept"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("", srv.URL, "")

	assert.Nil(t, err)
}

func TestSource_SetsAuthorizationHeaderIfAccessTokenProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("eyJ...", srv.URL, "")

	assert.Nil(t, err)
}

func TestSource_DoesNotSetAuthorizationHeaderIfAccessTokenNotProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Empty(t, r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("", srv.URL, "")

	assert.Nil(t, err)
}

func TestSource_UnmarshalsCorrectly(t *testing.T) {
	expected := "https://foo.bar"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["type"] = []string{"h-foo"}
		properties := make(map[string]any)
		body["properties"] = properties
		properties["listen-of"] = []string{expected}

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := Source("", srv.URL, "")

	assert.Nil(t, err)
	assert.Contains(t, res.Type, "h-foo")
	assert.Equal(t, []interface{}{expected}, res.Properties["listen-of"])
}

func TestSource_HandlesAbsenseOfKeys(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	res, err := Source("", srv.URL, "")

	assert.Nil(t, err)
	assert.NotNil(t, res)
}

func TestSource_IgnoresUnsupportedProperties(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["_unknown"] = "foo"

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := Source("", srv.URL, "")

	assert.Nil(t, err)
	assert.NotNil(t, res)
}

func TestSource_ReturnsErrorWhenNot200(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Source("", srv.URL, "https://bar")

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s?q=source&url=https%%3A%%2F%%2Fbar returned an HTTP %d error, with body: \n%s", srv.URL, 400, "{}")
	}
}
