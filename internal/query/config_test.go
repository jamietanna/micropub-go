package query

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig_SendsGET(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "GET", r.Method)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("", srv.URL)

	assert.Nil(t, err)
}
func TestConfig_SetsQuery(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "q=config", r.URL.RawQuery)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("", srv.URL)

	assert.Nil(t, err)
}
func TestConfig_SetsAcceptHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "application/json", r.Header.Get("accept"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("", srv.URL)

	assert.Nil(t, err)
}

func TestConfig_SetsAuthorizationHeaderIfAccessTokenProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("eyJ...", srv.URL)

	assert.Nil(t, err)
}

func TestConfig_DoesNotSetAuthorizationHeaderIfAccessTokenNotProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Empty(t, r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("", srv.URL)

	assert.Nil(t, err)
}

func TestConfig_UnmarshalsCorrectly(t *testing.T) {
	expected := "https://foo.bar"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["media-endpoint"] = expected

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := Config("", srv.URL)

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, expected, res.MediaEndpoint)
}

func TestConfig_HandlesAbsenseOfKeys(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	res, err := Config("", srv.URL)

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Empty(t, res.MediaEndpoint)
}

func TestConfig_IgnoresUnsupportedProperties(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["_unknown"] = "foo"

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := Config("", srv.URL)

	assert.Nil(t, err)
	assert.NotNil(t, res)
}

func TestConfig_ReturnsErrorWhenNot200(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := Config("", srv.URL)

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s?q=config returned an HTTP %d error, with body: \n%s", srv.URL, 400, "{}")
	}
}
