package query

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenericQuery_SendsGET(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "GET", r.Method)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQuery_SetsQueryWhenMultiple(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		values := r.URL.Query()["foo[]"]

		assert.Len(t, values, 2)
		assert.Contains(t, values, "bar")
		assert.Contains(t, values, "baz")
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"foo", "foo[]=bar", "foo[]=baz"})

	assert.Nil(t, err)
}

func TestGenericQuery_SetsQuery(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "config", r.URL.Query().Get("q"))
		assert.Equal(t, "bar", r.URL.Query().Get("foo"))
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"config", "foo=bar"})

	assert.Nil(t, err)
}
func TestGenericQuery_SetsAcceptHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "application/json", r.Header.Get("accept"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQuery_SetsAuthorizationHeaderIfAccessTokenProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("eyJ...", srv.URL, []string{"query"})

	assert.Nil(t, err)
}
func TestGenericQuery_DoesNotSetAuthorizationHeaderIfAccessTokenNotProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Empty(t, r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQuery_UnmarshalsCorrectly(t *testing.T) {
	expected := "https://foo.bar"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["media-endpoint"] = expected

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, expected, res["media-endpoint"])
}

func TestGenericQuery_HandlesAbsenseOfKeys(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	res, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Empty(t, res)
}

func TestGenericQuery_IgnoresUnsupportedProperties(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body := make(map[string]any)
		body["_unknown"] = "foo"

		j, err := json.Marshal(body)
		assert.Nil(t, err)
		_, _ = w.Write(j)
	}))
	defer srv.Close()

	res, err := GenericQuery("", srv.URL, []string{"query"})

	assert.Nil(t, err)
	assert.NotNil(t, res)
}

func TestGenericQuery_ReturnsErrorWhenNot200(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQuery("", srv.URL, []string{"query"})

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s?q=query returned an HTTP %d error, with body: \n%s", srv.URL, 400, "{}")
	}
}

func TestGenericQueryRaw_SendsGET(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "GET", r.Method)
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQueryRaw_SetsQueryWhenMultiple(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		values := r.URL.Query()["foo[]"]

		assert.Len(t, values, 2)
		assert.Contains(t, values, "bar")
		assert.Contains(t, values, "baz")
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"foo", "foo[]=bar", "foo[]=baz"})

	assert.Nil(t, err)
}

func TestGenericQueryRaw_SetsQuery(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "config", r.URL.Query().Get("q"))
		assert.Equal(t, "bar", r.URL.Query().Get("foo"))
		w.WriteHeader(200)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"config", "foo=bar"})

	assert.Nil(t, err)
}
func TestGenericQueryRaw_SetsAcceptHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "application/json", r.Header.Get("accept"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQueryRaw_SetsAuthorizationHeaderIfAccessTokenProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("eyJ...", srv.URL, []string{"query"})

	assert.Nil(t, err)
}
func TestGenericQueryRaw_DoesNotSetAuthorizationHeaderIfAccessTokenNotProvided(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Empty(t, r.Header.Get("authorization"))
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"query"})

	assert.Nil(t, err)
}

func TestGenericQueryRaw_UnmarshalsCorrectly(t *testing.T) {
	expected := "this is the raw body"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(expected))
	}))
	defer srv.Close()

	res, err := GenericQueryRaw("", srv.URL, []string{"query"})

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, expected, string(res))
}

func TestGenericQueryRaw_ReturnsErrorWhenNot200(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(400)
		_, _ = w.Write([]byte("{}"))
	}))
	defer srv.Close()

	_, err := GenericQueryRaw("", srv.URL, []string{"query"})

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s?q=query returned an HTTP %d error, with body: \n%s", srv.URL, 400, "{}")
	}
}
