package query

import (
	"encoding/json"

	"www.jvt.me/go/micropub/domain/post"
)

// Source Perform the q=source query for the Micropub server, for a given post
func Source(accessToken string, endpoint string, URL string) (*post.Post, error) {
	rBody, err := GenericQueryRaw(accessToken, endpoint, []string{"source", "url=" + URL})
	if err != nil {
		return nil, err
	}

	post := post.Post{}
	err = json.Unmarshal(rBody, &post)
	if err != nil {
		return nil, err
	}
	return &post, nil
}
