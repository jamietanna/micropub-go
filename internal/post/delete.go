package post

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

// UpdateResponse The response from a Delete/Undelete/Update request
type UpdateResponse struct {
	Location string
}

// DeleteForm Delete a post using the form POST method
func DeleteForm(accessToken string, endpoint string, URL string) (*UpdateResponse, error) {
	return deleteUndeleteForm("delete", accessToken, endpoint, URL)
}

// UndeleteForm Undelete a post using the form POST method
func UndeleteForm(accessToken string, endpoint string, URL string) (*UpdateResponse, error) {
	return deleteUndeleteForm("undelete", accessToken, endpoint, URL)
}

func deleteUndeleteForm(action string, accessToken string, endpoint string, URL string) (*UpdateResponse, error) {
	client := &http.Client{}

	body := url.Values{
		"action": {action},
		"url":    {URL},
	}

	req, err := http.NewRequest("POST", endpoint, strings.NewReader(body.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 200 || resp.StatusCode == 201 || resp.StatusCode == 204 {
		return &UpdateResponse{Location: resp.Header.Get("Location")}, nil
	}

	defer resp.Body.Close()
	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, with body: \n%s", endpoint, resp.StatusCode, string([]byte(rBody)))
}
