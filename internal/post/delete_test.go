package post

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeleteForm_ReturnsResponse(t *testing.T) {
	cases := []struct {
		name string
		code int
	}{
		{
			name: "When 200",
			code: 200,
		},
		{
			name: "When 201",
			code: 201,
		},
		{
			name: "When204",
			code: 204,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(c.code)
			}))
			defer srv.Close()

			res, err := DeleteForm("", srv.URL, "")

			assert.Nil(t, err)
			assert.NotNil(t, res)
		})
	}
}

func TestDeleteForm_ReturnsLocationHeaderIfPresent(t *testing.T) {
	cases := []struct {
		name string
		code int
	}{
		{
			name: "When 200",
			code: 200,
		},
		{
			name: "When 201",
			code: 201,
		},
		{
			name: "When204",
			code: 204,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("location", "https://new-url")
				w.WriteHeader(c.code)
			}))
			defer srv.Close()

			res, err := DeleteForm("", srv.URL, "")

			assert.Nil(t, err)
			assert.Equal(t, "https://new-url", res.Location)
		})
	}
}

func TestDeleteForm_IsAPost(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "POST", r.Method)
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := DeleteForm("", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestDeleteForm_SetsBodyParameters(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		assert.Nil(t, err, "Unexpected error when parsing form data")

		assert.Equal(t, "delete", r.PostForm.Get("action"))
		assert.Equal(t, "https://foo", r.PostForm.Get("url"))
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := DeleteForm("", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestDeleteForm_SetsAuthorizationHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := DeleteForm("eyJ...", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestDeleteForm_ReturnsErrorWhenUnexpectedStatusCode(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("body"))
	}))
	defer srv.Close()

	_, err := DeleteForm("", srv.URL, "")

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s returned an HTTP %d error, with body: \n%s", srv.URL, 404, "body")
	}
}

func TestUndeleteForm_ReturnsResponse(t *testing.T) {
	cases := []struct {
		name string
		code int
	}{
		{
			name: "When 200",
			code: 200,
		},
		{
			name: "When 201",
			code: 201,
		},
		{
			name: "When204",
			code: 204,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(c.code)
			}))
			defer srv.Close()

			res, err := UndeleteForm("", srv.URL, "")

			assert.Nil(t, err)
			assert.NotNil(t, res)
		})
	}
}

func TestUndeleteForm_ReturnsLocationHeaderIfPresent(t *testing.T) {
	cases := []struct {
		name string
		code int
	}{
		{
			name: "When 200",
			code: 200,
		},
		{
			name: "When 201",
			code: 201,
		},
		{
			name: "When204",
			code: 204,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("location", "https://new-url")
				w.WriteHeader(c.code)
			}))
			defer srv.Close()

			res, err := UndeleteForm("", srv.URL, "")

			assert.Nil(t, err)
			assert.Equal(t, "https://new-url", res.Location)
		})
	}
}

func TestUndeleteForm_IsAPost(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "POST", r.Method)
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := UndeleteForm("", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestUndeleteForm_SetsBodyParameters(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		assert.Nil(t, err, "Unexpected error when parsing form data")

		assert.Equal(t, "undelete", r.PostForm.Get("action"))
		assert.Equal(t, "https://foo", r.PostForm.Get("url"))
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := UndeleteForm("", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestUndeleteForm_SetsAuthorizationHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		w.WriteHeader(200)
	}))
	defer srv.Close()

	_, err := UndeleteForm("eyJ...", srv.URL, "https://foo")

	assert.Nil(t, err)
}

func TestUndeleteForm_ReturnsErrorWhenUnexpectedStatusCode(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("body"))
	}))
	defer srv.Close()

	_, err := UndeleteForm("", srv.URL, "")

	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s returned an HTTP %d error, with body: \n%s", srv.URL, 404, "body")
	}
}
