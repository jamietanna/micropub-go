package post

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func (mockPost) ToJSON() ([]byte, error) {
	return []byte("to-json-result"), nil
}

func TestCreateJSON(t *testing.T) {
	t.Run("returns location", func(t *testing.T) {
		cases := []struct {
			name string
			code int
		}{
			{
				name: "When 201",
				code: 201,
			},
			{
				name: "When 202",
				code: 202,
			},
		}

		for _, c := range cases {
			t.Run(c.name, func(t *testing.T) {
				expected := "https://url"

				srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Header().Add("Location", expected)
					w.WriteHeader(c.code)
				}))
				defer srv.Close()

				res, err := CreateJSON("", srv.URL, mockPost{})
				assert.Nil(t, err)
				assert.Equal(t, expected, res.Location)
			})
		}
	})

	t.Run("sets content-type as JSON", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Len(t, r.Header["Content-Type"], 1)
			assert.Contains(t, r.Header["Content-Type"], "application/json")
			w.WriteHeader(201)
		}))
		defer srv.Close()

		mp := mockPost{}
		_, err := CreateJSON("", srv.URL, mp)

		assert.Nil(t, err)
	})
	t.Run("sets body as result of post.ToJSON", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, err := io.ReadAll(r.Body)
			assert.Nil(t, err, "Unexpected error when parsing request body")

			assert.Equal(t, []byte("to-json-result"), body)
			w.WriteHeader(201)
		}))
		defer srv.Close()

		mp := mockPost{}
		_, err := CreateJSON("", srv.URL, mp)

		assert.Nil(t, err)
	})

	t.Run("adds access token to Authorization header", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fmt.Println(r.Header)
			assert.Len(t, r.Header["Authorization"], 1)
			assert.Contains(t, r.Header["Authorization"], "Bearer eyJ...")
			w.WriteHeader(201)
		}))
		defer srv.Close()

		_, err := CreateJSON("eyJ...", srv.URL, mockPost{})

		assert.Nil(t, err)
	})

	t.Run("returns error when not 201 or 202", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(404)
			_, _ = w.Write([]byte("body"))
		}))
		defer srv.Close()

		_, err := CreateJSON("eyJ...", srv.URL, mockPost{})
		if assert.NotNil(t, err) {
			assert.Errorf(t, err, "the Micropub server at %s returned an HTTP %d error, with body: \n%s", srv.URL, 404, "body")
		}
	})
}
