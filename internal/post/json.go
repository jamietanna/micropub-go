package post

import (
	"bytes"
	"fmt"
	"io"
	"net/http"

	"www.jvt.me/go/micropub/domain/post"
)

// CreateJSON Create a post using a JSON-encoded body
func CreateJSON(accessToken string, endpoint string, post post.IPost) (*MicropubEndpointResponse, error) {
	client := &http.Client{}

	jsonBytes, err := post.ToJSON()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", endpoint, bytes.NewReader(jsonBytes))
	if err != nil {
		return nil, err
	}

	req.Header.Set("content-type", "application/json")
	req.Header.Set("authorization", "Bearer "+accessToken)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 201 || resp.StatusCode == 202 {
		return &MicropubEndpointResponse{Location: resp.Header.Get("Location")}, nil
	}

	defer resp.Body.Close()
	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, but there was an issue parsing the body", endpoint, resp.StatusCode)
	}

	return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, with body: \n%s", endpoint, resp.StatusCode, string([]byte(rBody)))
}
