package post

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"www.jvt.me/go/micropub/domain/post"
)

type mockPost struct {
	post.IPost
	mockForm *url.Values
}

func (m mockPost) ToForm() *url.Values {
	if m.mockForm == nil {
		return &url.Values{}
	}
	return m.mockForm
}

func TestCreateForm(t *testing.T) {
	t.Run("returns location", func(t *testing.T) {
		cases := []struct {
			name string
			code int
		}{
			{
				name: "When 201",
				code: 201,
			},
			{
				name: "When 202",
				code: 202,
			},
		}

		for _, c := range cases {
			t.Run(c.name, func(t *testing.T) {
				expected := "https://url"

				srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.Header().Add("Location", expected)
					w.WriteHeader(c.code)
				}))
				defer srv.Close()

				res, err := CreateForm("", srv.URL, mockPost{}, nil)
				assert.Nil(t, err)
				assert.Equal(t, expected, res.Location)
			})
		}
	})

	t.Run("sets body parameters", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm()
			assert.Nil(t, err, "Unexpected error when parsing form data")

			assert.Equal(t, r.PostForm.Get("h"), "entry")
			w.WriteHeader(201)
		}))
		defer srv.Close()

		mp := mockPost{
			mockForm: &url.Values{"h": {"entry"}},
		}
		_, err := CreateForm("", srv.URL, mp, nil)

		assert.Nil(t, err)
	})

	t.Run("handles absent values in commands", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm()
			assert.Nil(t, err, "Unexpected error when parsing form data")

			assert.NotContains(t, r.PostForm, "mp-slug")
			w.WriteHeader(201)
		}))
		defer srv.Close()

		mp := mockPost{
			mockForm: &url.Values{"h": {"entry"}},
		}
		commands := make(post.Commands)
		commands["mp-slug"] = []string{}

		_, err := CreateForm("", srv.URL, mp, commands)

		assert.Nil(t, err)
	})

	t.Run("sets commands, if present", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm()
			assert.Nil(t, err, "Unexpected error when parsing form data")

			assert.Equal(t, r.PostForm.Get("h"), "entry")
			assert.Equal(t, r.PostForm.Get("mp-slug"), "single-value")
			assert.Contains(t, r.PostForm, "mp-syndicate-to[]")
			assert.Equal(t, r.PostForm["mp-syndicate-to[]"], []string{"https://1", "https://2"})
			w.WriteHeader(201)
		}))
		defer srv.Close()

		mp := mockPost{
			mockForm: &url.Values{"h": {"entry"}},
		}
		commands := make(post.Commands)
		commands["mp-slug"] = []string{"single-value"}
		commands["mp-syndicate-to"] = []string{"https://1", "https://2"}

		_, err := CreateForm("", srv.URL, mp, commands)

		assert.Nil(t, err)
	})

	t.Run("adds access token to body", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := r.ParseForm()
			assert.Nil(t, err, "Unexpected error when parsing form data")

			assert.Equal(t, "eyJ...", r.PostForm.Get("access_token"))
			w.WriteHeader(201)
		}))
		defer srv.Close()

		_, err := CreateForm("eyJ...", srv.URL, mockPost{}, nil)

		assert.Nil(t, err)
	})

	t.Run("returns error when not 201 or 202", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(404)
			_, _ = w.Write([]byte("body"))
		}))
		defer srv.Close()

		_, err := CreateForm("eyJ...", srv.URL, mockPost{}, nil)
		if assert.NotNil(t, err) {
			assert.Errorf(t, err, "the Micropub server at %s returned an HTTP %d error, with body: \n%s", srv.URL, 404, "body")
		}
	})
}
