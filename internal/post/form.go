package post

import (
	"fmt"
	"io"
	"net/http"

	"www.jvt.me/go/micropub/domain/post"
)

// MicropubEndpointResponse The response rom the Micropub endpoint
type MicropubEndpointResponse struct {
	Location string
}

// CreateForm Create a post using the form POST method
func CreateForm(accessToken string, endpoint string, post post.IPost, commands post.Commands) (*MicropubEndpointResponse, error) {
	client := &http.Client{}

	body := post.ToForm()
	body.Add("access_token", accessToken)
	for k, v := range commands {
		if len(v) == 1 {
			body.Add(k, v[0])
		} else {
			for _, v2 := range v {
				body.Add(k+"[]", v2)
			}
		}
	}

	resp, err := client.PostForm(endpoint, *body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 201 || resp.StatusCode == 202 {
		return &MicropubEndpointResponse{Location: resp.Header.Get("Location")}, nil
	}

	defer resp.Body.Close()
	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, but there was an issue parsing the body", endpoint, resp.StatusCode)
	}

	return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, with body: \n%s", endpoint, resp.StatusCode, string([]byte(rBody)))
}
