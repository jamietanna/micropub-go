package media

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

// Response The response from the Media endpoint
type Response struct {
	Location string
}

// UploadFile Upload a given file to the Media Endpoint
func UploadFile(accessToken string, endpoint string, filePath string) (*Response, error) {
	if len(endpoint) == 0 {
		return nil, errors.New("the Micropub server does not advertise/support a Media Endpoint")
	}

	client := &http.Client{}

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	fw, err := writer.CreateFormFile("file", filePath)
	if err != nil {
		return nil, err
	}
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(fw, file)
	if err != nil {
		return nil, err
	}

	writer.Close()

	req, err := http.NewRequest("POST", endpoint, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 201 {
		return &Response{Location: resp.Header.Get("Location")}, nil
	}

	rBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, but there was an issue parsing the body", endpoint, resp.StatusCode)
	}
	return nil, fmt.Errorf("the Micropub server at %s returned an HTTP %d error, with body: \n%s", endpoint, resp.StatusCode, string([]byte(rBody)))
}
