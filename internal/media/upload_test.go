package media

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUploadFile_ReturnsLocationWhen201(t *testing.T) {
	expected := "https://media.jpg"

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("location", expected)
		w.WriteHeader(201)
	}))
	defer srv.Close()

	res, err := UploadFile("", srv.URL, "upload_test.go")

	assert.Nil(t, err)
	assert.Equal(t, expected, res.Location)
}

func TestUploadFile_SetsBodyParameters(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseMultipartForm(10_000)
		assert.Nil(t, err, "Unexpected error when parsing form data")

		value, ok := r.MultipartForm.File["file"]
		if !ok {
			t.Error("Expected there to be a `file` file in the request")
		}
		file := *value[0]
		assert.Equal(t, "upload_test.go", file.Filename)

		w.WriteHeader(201)
	}))
	defer srv.Close()

	_, err := UploadFile("", srv.URL, "upload_test.go")

	assert.Nil(t, err)
}

func TestUploadFile_SetsAuthorizationHeader(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, "Bearer eyJ...", r.Header.Get("authorization"))
		w.WriteHeader(201)
	}))
	defer srv.Close()

	_, err := UploadFile("eyJ...", srv.URL, "upload_test.go")

	assert.Nil(t, err)
}

func TestUploadFile_SetsContentType(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.Header.Get("content-type"))
		assert.True(t, strings.HasPrefix(r.Header.Get("content-type"), "multipart/form-data"))
		w.WriteHeader(201)
	}))
	defer srv.Close()

	_, err := UploadFile("eyJ...", srv.URL, "upload_test.go")

	assert.Nil(t, err)
}

func TestUploadFile_ReturnsErrorWhenNot201(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
		_, _ = w.Write([]byte("body"))
	}))
	defer srv.Close()

	_, err := UploadFile("", srv.URL, "upload_test.go")
	if assert.NotNil(t, err) {
		assert.Errorf(t, err, "the Micropub server at %s returned an HTTP %d error, with body: \n%s", srv.URL, 404, "body")
	}
}

func TestUploadFile_ReturnsErrorWhenNoEndpointProvided(t *testing.T) {
	_, err := UploadFile("", "", "")
	if assert.NotNil(t, err) {
		assert.Error(t, err, "the Micropub server does not advertise/support a Media Endpoint")
	}
}
