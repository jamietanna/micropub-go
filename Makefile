GOBASE=$(shell pwd)
GOBIN=$(GOBASE)/bin

$(GOBIN)/golangci-lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(GOBIN) v1.50.1

lint: $(GOBIN)/golangci-lint
	$(GOBIN)/golangci-lint run ./...

test:
	@if [ -n "$(CI)" ]; then \
		go test -coverprofile=coverage.out -json ./... | tee test-report.json; \
		else \
		go test ./...; \
		fi
