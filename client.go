package micropub

import (
	p "www.jvt.me/go/micropub/domain/post"

	"www.jvt.me/go/micropub/internal/media"
	"www.jvt.me/go/micropub/internal/post"
	"www.jvt.me/go/micropub/internal/query"
)

// Client A client to interact with a given Micropub server
type Client struct {
	Endpoint      string
	MediaEndpoint string
	AccessToken   string
}

// GenericQuery Perform a given query to the Micropub server
func (c *Client) GenericQuery(args []string) (map[string]any, error) {
	return query.GenericQuery(c.AccessToken, c.Endpoint, args)
}

// QueryConfig Perform the q=config query for the Micropub server
func (c *Client) QueryConfig() (*query.MicropubConfig, error) {
	return query.Config(c.AccessToken, c.Endpoint)
}

// QuerySource Perform the q=source query for the Micropub server, for a given post
func (c *Client) QuerySource(URL string) (*p.Post, error) {
	return query.Source(c.AccessToken, c.Endpoint, URL)
}

// CreateForm Create a post using the form POST method
func (c *Client) CreateForm(thePost *p.Post, commands p.Commands) (*post.MicropubEndpointResponse, error) {
	return post.CreateForm(c.AccessToken, c.Endpoint, thePost, commands)
}

// CreateJSON Create a post using a JSON-encoded body
func (c *Client) CreateJSON(thePost *p.Post) (*post.MicropubEndpointResponse, error) {
	return post.CreateJSON(c.AccessToken, c.Endpoint, thePost)
}

// DeleteForm Delete a post using the form POST method
func (c *Client) DeleteForm(URL string) (*post.UpdateResponse, error) {
	return post.DeleteForm(c.AccessToken, c.Endpoint, URL)
}

// UndeleteForm Undelete a post using the form POST method
func (c *Client) UndeleteForm(URL string) (*post.UpdateResponse, error) {
	return post.UndeleteForm(c.AccessToken, c.Endpoint, URL)
}

// UploadFile Upload a given file to the Media Endpoint
func (c *Client) UploadFile(filePath string) (*media.Response, error) {
	return media.UploadFile(c.AccessToken, c.MediaEndpoint, filePath)
}
