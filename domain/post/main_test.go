package post

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFromStrings(t *testing.T) {
	t.Run("Returns error when empty", func(t *testing.T) {
		_, _, err := FromStrings([]string{})

		if assert.NotNil(t, err) {
			assert.Error(t, err, "no h value or properties provided")
		}
	})

	t.Run("Handles single property", func(t *testing.T) {
		post, _, err := FromStrings([]string{"category=foo"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "foo")
	})

	t.Run("Assumes h=entry if not set", func(t *testing.T) {
		post, _, err := FromStrings([]string{"category=foo"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Type
		assert.Len(t, category, 1)
		assert.Contains(t, category, "h-entry")
	})

	t.Run("Allows specifying h= value", func(t *testing.T) {
		post, _, err := FromStrings([]string{"h=h-h-card", "category=foo"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Type
		assert.Len(t, category, 1)
		assert.Contains(t, category, "h-h-h-card")
	})

	t.Run("Handles single property with multiple values, when duplicate key names", func(t *testing.T) {
		post, _, err := FromStrings([]string{"category=foo", "category=bar"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles single property with multiple values, when using [] syntax", func(t *testing.T) {
		post, _, err := FromStrings([]string{"category[]=foo", "category[]=bar"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles single property with multiple values, when using a mix of []s and no []s", func(t *testing.T) {
		post, _, err := FromStrings([]string{"category[]=foo", "category=bar"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles multiple properties", func(t *testing.T) {
		post, _, err := FromStrings([]string{"url=https://foo", "category=bar"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		url := post.Properties["url"]
		assert.Len(t, url, 1)
		assert.Contains(t, url, "https://foo")
		category := post.Properties["category"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles equals in parameter name", func(t *testing.T) {
		post, _, err := FromStrings([]string{"rel=twitter=JamieTanna=foo=bar"})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["rel=twitter"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "JamieTanna=foo=bar")
	})

	t.Run("Ignores access_token", func(t *testing.T) {
		post, _, err := FromStrings([]string{"access_token=eyJ..."})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		assert.Empty(t, post.Properties)
	})

	t.Run("returns empty commands if none present", func(t *testing.T) {
		_, commands, err := FromStrings([]string{"h=entry"})

		assert.Nil(t, err)
		assert.Empty(t, commands)
	})

	t.Run("returns mp- commands if present", func(t *testing.T) {
		_, commands, err := FromStrings([]string{"h=entry", "mp-slug=foo"})

		assert.Nil(t, err)
		assert.Len(t, commands, 1)
		assert.Contains(t, commands, "mp-slug")
		slug := commands["mp-slug"]
		assert.Len(t, slug, 1)
		assert.Equal(t, slug, []string{"foo"})
	})

	t.Run("returns mp- commands if present, and multi-value", func(t *testing.T) {
		_, commands, err := FromStrings([]string{"h=entry", "mp-syndicate-to[]=https://foo", "mp-syndicate-to[]=https://bar"})

		assert.Nil(t, err)
		assert.Len(t, commands, 1)
		assert.Contains(t, commands, "mp-syndicate-to")
		slug := commands["mp-syndicate-to"]
		assert.Len(t, slug, 2)
		assert.Equal(t, slug, []string{"https://foo", "https://bar"})
	})
}

func TestFromForm(t *testing.T) {
	t.Run("Returns error when empty", func(t *testing.T) {
		_, _, err := FromForm(&url.Values{})

		if assert.NotNil(t, err) {
			assert.Contains(t, err.Error(), "no h value or properties provided")
		}
	})

	t.Run("Handles single property", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"category": {"foo"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "foo")
	})

	t.Run("Assumes h=entry if not set", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"category": {"foo"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Type
		assert.Len(t, category, 1)
		assert.Contains(t, category, "h-entry")
	})

	t.Run("Allows specifying h= value", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"h": {"h-h-card"}, "category": {"foo"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Type
		assert.Len(t, category, 1)
		assert.Contains(t, category, "h-h-h-card")
	})

	t.Run("Handles single property with multiple values, when duplicate key names", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"category": {"foo", "bar"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles single property with multiple values, when using [] syntax", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"category[]": {"foo", "bar"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles single property with multiple values, when using a mix of []s and no []s", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"category[]": {"foo"}, "category": {"bar"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["category"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles multiple properties", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"url": {"https://foo"}, "category": {"bar"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		url := post.Properties["url"]
		assert.Len(t, url, 1)
		assert.Contains(t, url, "https://foo")
		category := post.Properties["category"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "bar")
	})

	t.Run("Handles equals in parameter name", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"rel=twitter": {"JamieTanna=foo=bar"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		category := post.Properties["rel=twitter"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "JamieTanna=foo=bar")
	})

	t.Run("Ignores access_token", func(t *testing.T) {
		post, _, err := FromForm(&url.Values{"access_token": {"eyJ"}})

		assert.Nil(t, err)
		assert.NotNil(t, post)
		assert.Empty(t, post.Properties)
	})

	t.Run("returns mp- commands if present", func(t *testing.T) {
		_, commands, err := FromForm(&url.Values{"h": {"entry"}, "mp-slug": {"foo"}})

		assert.Nil(t, err)
		assert.Len(t, commands, 1)
		assert.Contains(t, commands, "mp-slug")
		slug := commands["mp-slug"]
		assert.Len(t, slug, 1)
		assert.Equal(t, slug, []string{"foo"})
	})

	t.Run("returns mp- commands if present, and multi-value", func(t *testing.T) {
		_, commands, err := FromForm(&url.Values{"h": {"entry"}, "mp-syndicate-to": {"https://foo", "https://bar"}})

		assert.Nil(t, err)
		assert.Len(t, commands, 1)
		assert.Contains(t, commands, "mp-syndicate-to")
		slug := commands["mp-syndicate-to"]
		assert.Len(t, slug, 2)
		assert.Equal(t, slug, []string{"https://foo", "https://bar"})
	})
}

func TestToForm(t *testing.T) {
	t.Run("When no type", func(t *testing.T) {
		p := Post{}

		values := *p.ToForm()
		assert.Empty(t, values)
	})

	t.Run("It strips h- prefix from type", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}

		values := *p.ToForm()
		h := values["h"]
		assert.Len(t, h, 1)
		assert.Contains(t, h, "entry")
	})

	t.Run("When more than one type, only the first is retained", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry", "h-event"}

		values := *p.ToForm()
		h := values["h"]
		assert.Len(t, h, 1)
	})

	t.Run("When single property", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}
		p.Properties = make(map[string][]interface{})
		p.Properties["url"] = []interface{}{"url"}

		values := *p.ToForm()
		url := values["url"]
		assert.Len(t, url, 1)
		assert.Contains(t, url, "url")
	})

	t.Run("Handles single property with multiple values", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}
		p.Properties = make(map[string][]interface{})
		p.Properties["category"] = []interface{}{"foo", "bar"}

		values := *p.ToForm()
		category := values["category[]"]
		assert.Len(t, category, 2)
		assert.Contains(t, category, "foo")
		assert.Contains(t, category, "bar")
	})

	t.Run("When multiple properties", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}
		p.Properties = make(map[string][]interface{})
		p.Properties["category"] = []interface{}{"foo"}
		p.Properties["repost-of"] = []interface{}{"https://theirs"}

		values := *p.ToForm()
		repostOf := values["repost-of"]
		assert.Len(t, repostOf, 1)
		assert.Contains(t, repostOf, "https://theirs")
		category := values["category"]
		assert.Len(t, category, 1)
		assert.Contains(t, category, "foo")
	})

	t.Run("When equals in parameter name", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}
		p.Properties = make(map[string][]interface{})
		p.Properties["rel=twitter"] = []interface{}{"JamieTanna"}

		values := *p.ToForm()
		repostOf := values["rel=twitter"]
		assert.Len(t, repostOf, 1)
		assert.Contains(t, repostOf, "JamieTanna")
	})

	t.Run("When many equals signs", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-entry"}
		p.Properties = make(map[string][]interface{})
		p.Properties["rel=twitter"] = []interface{}{"JamieTanna=foo=bar=baz"}

		values := *p.ToForm()
		repostOf := values["rel=twitter"]
		assert.Len(t, repostOf, 1)
		assert.Contains(t, repostOf, "JamieTanna=foo=bar=baz")
	})
}

func TestToJSON(t *testing.T) {
	t.Run("When no type", func(t *testing.T) {
		p := Post{}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Contains(t, string(bytes), `{"type":null,"properties":null}`)
	})

	t.Run("When type", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-measure"}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Contains(t, string(bytes), `{"type":["h-measure"],"properties":null}`)
	})

	t.Run("When multiple types", func(t *testing.T) {
		p := Post{}
		p.Type = []string{"h-measure", "h-card"}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Contains(t, string(bytes), `{"type":["h-measure","h-card"],"properties":null}`)
	})

	t.Run("When single property", func(t *testing.T) {
		p := Post{}
		p.Properties = make(map[string][]interface{})
		p.Properties["url"] = []interface{}{"url"}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Equal(t, `{"type":null,"properties":{"url":["url"]}}`, string(bytes))
	})

	t.Run("When single property, with multiple values", func(t *testing.T) {
		p := Post{}
		p.Properties = make(map[string][]interface{})
		p.Properties["url"] = []interface{}{"url", "https://"}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Equal(t, `{"type":null,"properties":{"url":["url","https://"]}}`, string(bytes))
	})

	t.Run("When multiple properties", func(t *testing.T) {
		p := Post{}
		p.Properties = make(map[string][]interface{})
		p.Properties["url"] = []interface{}{"url"}
		p.Properties["category"] = []interface{}{"category"}

		bytes, err := p.ToJSON()
		assert.Nil(t, err)
		assert.Equal(t, `{"type":null,"properties":{"category":["category"],"url":["url"]}}`, string(bytes))
	})
}
