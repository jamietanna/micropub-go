package post

import (
	"encoding/json"
	"errors"
	"net/url"
	"strings"

	"willnorris.com/go/microformats"
)

// Post The representation of a post
type Post struct {
	microformats.Microformat
}

// IPost The representation of a post
type IPost interface {
	ToForm() *url.Values
	ToJSON() ([]byte, error)
}

// Commands are the Micropub commands parsed from the request body, which are multi-value, and normalised by removing any `[]`s in the name
type Commands map[string][]string

// FromStrings Create a Post from a given set of key-value arguments, such as:
//
//	[]string{"h=entry", "category=bar", "category[]=baz"}
func FromStrings(args []string) (*Post, Commands, error) {
	if len(args) == 0 {
		return nil, nil, errors.New("no h value or properties provided")
	}

	commands := make(Commands)

	p := Post{}
	p.Properties = make(map[string][]interface{})

	for _, arg := range args {
		parts := strings.Split(arg, "=")
		if parts[0] == "h" {
			p.Type = []string{"h-" + parts[1]}
			continue
		}
		if parts[0] == "access_token" {
			continue
		}

		var key, val string
		if len(parts) >= 3 {
			key = parts[0] + "=" + parts[1]
			val = strings.Join(parts[2:], "=")
		} else {
			key = parts[0]
			val = parts[1]
		}
		key = strings.ReplaceAll(key, "[]", "")
		if strings.HasPrefix(key, "mp-") {
			commands[key] = append(commands[key], val)
		} else {
			p.Properties[key] = append(p.Properties[key], val)
		}
	}
	if len(p.Type) == 0 {
		p.Type = []string{"h-entry"}
	}

	return &p, commands, nil
}

// FromForm Create a Post from a given set of url.Values, such as:
//
//	&url.Values{"h": {"entry"}, "category": {"foo"}}
//
// Best used when parsing the incoming HTTP request
func FromForm(values *url.Values) (*Post, Commands, error) {
	if len(*values) == 0 {
		return nil, nil, errors.New("no h value or properties provided")
	}

	commands := make(Commands)
	p := Post{}
	p.Properties = make(map[string][]interface{})

	for k, arg := range *values {
		// parts := strings.Split(arg, "=")
		if k == "h" {
			p.Type = []string{"h-" + arg[0]}
			continue
		}
		if k == "access_token" {
			continue
		}

		key := strings.ReplaceAll(k, "[]", "")

		for _, val := range arg {
			// p.Properties[key] = append(p.Properties[key], val)
			if strings.HasPrefix(key, "mp-") {
				commands[key] = append(commands[key], val)
			} else {
				p.Properties[key] = append(p.Properties[key], val)
			}
		}
	}
	if len(p.Type) == 0 {
		p.Type = []string{"h-entry"}
	}

	return &p, commands, nil
}

// ToForm Convert the given Post to a the form-encoded format of the post
func (p Post) ToForm() *url.Values {
	values := url.Values{}
	for key, val := range p.Properties {
		if len(val) > 1 {
			key = key + "[]"
		}
		values[key] = make([]string, len(val))
		for i, v := range val {
			values[key][i] = v.(string)
		}
	}

	if len(p.Type) > 0 {
		values["h"] = []string{strings.Replace(p.Type[0], "h-", "", 1)}
	}

	return &values
}

// ToJSON Convert the given Post to a the JSON format of the post
func (p Post) ToJSON() ([]byte, error) {
	return json.Marshal(p.Microformat)
}
