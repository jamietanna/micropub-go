package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:   "delete {url}",
	Short: "Delete posts",
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}
		client := config.client()
		resp, err := client.DeleteForm(args[0])
		if err != nil {
			return err
		}

		if len(resp.Location) > 0 {
			fmt.Println(resp.Location)
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(deleteCmd)
}
