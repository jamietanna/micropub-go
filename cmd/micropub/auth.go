package main

import (
	"github.com/spf13/cobra"
)

var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Manage your authentication",
}

func init() {
	rootCmd.AddCommand(authCmd)
}
