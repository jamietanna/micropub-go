package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/spf13/cobra"
)

var uploadCmd = &cobra.Command{
	Use:   "upload /path/to/file.png",
	Short: "Upload a specific file to the media endpoint",
	Long: `Upload a specific file to the media endpoint.

  Performs a multipart/form-data POST request with a provided file parameter.`,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}
		path, err := pathToUpload(args[0])
		if err != nil {
			return err
		}
		if len(config.MediaEndpoint) == 0 {
			config.MediaEndpoint = config.Endpoint
		}
		client := config.client()
		resp, err := client.UploadFile(path)
		if err != nil {
			return err
		}

		fmt.Println(resp.Location)
		return nil
	},
}

func pathToUpload(path string) (string, error) {
	u, err := url.Parse(path)
	if err != nil {
		return "", nil
	}
	if u.Scheme == "http" || u.Scheme == "https" {
		tmpfile, err := os.CreateTemp("", "micropub-")
		if err != nil {
			return "", err
		}

		defer tmpfile.Close()

		err = downloadToPath(u, tmpfile)
		if err != nil {
			return "", err
		}

		return tmpfile.Name(), nil
	}

	return path, nil
}

func downloadToPath(url *url.URL, file *os.File) error {
	resp, err := http.Get(url.String())
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf("the resource at %s returned an HTTP %d error", url.String(), resp.StatusCode)
	}

	_, err = os.Create(file.Name())
	if err != nil {
		return err
	}

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func init() {
	mediaCmd.AddCommand(uploadCmd)
}
