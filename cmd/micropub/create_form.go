package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"www.jvt.me/go/micropub/domain/post"
)

var formCmd = &cobra.Command{
	Use:   "form {h=entry content=hello}",
	Short: "Create a new post, using the form-encoded format",
	Long:  `Create a new post, using the form-encoded format`,
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}
		client := config.client()
		thePost, commands, err := post.FromStrings(args)
		if err != nil {
			return err
		}
		resp, err := client.CreateForm(thePost, commands)
		if err != nil {
			return err
		}

		fmt.Println(resp.Location)
		return nil
	},
}

func init() {
	createCmd.AddCommand(formCmd)
}
