package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"www.jvt.me/go/micropub/internal/mf2"
)

var discoverCmd = &cobra.Command{
	Use:   "discover",
	Short: "Discover Micropub endpoint(s)",
	Long: `Discover Micropub endpoint(s)

	This re-writes the configuration file for the profile, based on a
	rel=micropub lookup for the configured me.

	This also looks up the media endpoint using the q=config query.
  `,
	Args: cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		profileConfig, err := configForProfile(profile)
		if err != nil {
			return err
		}

		endpoint, err := mf2.DiscoverMicropubEndpoint(profileConfig.Me)
		if err != nil {
			return err
		}
		key := configurationPrefix(profile)
		viper.Set(key+".endpoint", endpoint)
		err = viper.WriteConfig()
		if err != nil {
			return err
		}

		client := profileConfig.client()
		config, err := client.QueryConfig()
		if err != nil {
			return err
		}

		viper.Set(key+".media_endpoint", config.MediaEndpoint)
		err = viper.WriteConfig()
		if err != nil {
			return err
		}
		fmt.Println("Successfully re-discovered endpoints")
		return nil
	},
}

func init() {
	configCmd.AddCommand(discoverCmd)
}
