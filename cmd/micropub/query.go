package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"

	"github.com/alecthomas/chroma/v2/quick"
	"github.com/spf13/cobra"
)

func prettyJSON(data *map[string]any) (string, error) {
	str, err := json.Marshal(data)
	if err != nil {
		return fmt.Sprintf("%s", data), nil
	}

	var prettyJSON bytes.Buffer
	if err := json.Indent(&prettyJSON, str, "", "  "); err != nil {
		return fmt.Sprintf("%s", data), nil
	}
	return prettyJSON.String(), nil
}

var queryCmd = &cobra.Command{
	Use:   "query {q parameter}",
	Short: "Perform Micropub queries",
	Long: `Perform Micropub queries

	The first argument will be the q= parameter in the query.

	Other arguments can be passed in i.e. query source url=https://...
	`,
	Args: cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}
		client := config.client()

		query, err := client.GenericQuery(args)
		if err != nil {
			return err
		}

		pretty, err := prettyJSON(&query)
		if err != nil {
			fmt.Println(query)
			return nil
		}
		err = quick.Highlight(os.Stdout, string(pretty), "json", "terminal256", "gruvbox")
		if err != nil {
			fmt.Println(query)
			return nil
		}

		return nil
	},
}

func init() {
	rootCmd.AddCommand(queryCmd)
}
