package main

import (
	"fmt"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialise a profile",
	Long: `Initialise a profile.

	Interactively configure a profile, and set up the configuration file accordingly.
  `,
	Args: cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		var qs = []*survey.Question{
			{
				Name: "me",
				Prompt: &survey.Input{
					Message: "What is your site's profile URL?",
					Help:    "This should be the URL that you would log in with, for instance `https://www.jvt.me/`",
				},
				Validate: survey.Required,
			},
			{
				Name: "profile",
				Prompt: &survey.Input{
					Message: "What should the profile be called?",
					Help:    "If you're not planning on using multiple identities or Micropub servers, `default` is sufficient. Otherwise, allows you more control",
					Default: "default",
				},
				Validate: survey.Required,
			},
		}
		answers := struct {
			Me      string
			Profile string
		}{}

		err := survey.Ask(qs, &answers)
		if err != nil {
			return err
		}

		key := configurationPrefix(answers.Profile)
		viper.Set(key+".me", answers.Me)
		err = viper.WriteConfig()
		if err != nil {
			return err
		}
		fmt.Printf("Successfully initialised profile %s for %s - run:\n\tmicropub auth reauth -p %s && micropub config discover -p %s\n", answers.Profile, answers.Me, answers.Profile, answers.Profile)

		return nil
	},
}

func init() {
	configCmd.AddCommand(initCmd)
}
