package main

import (
	"github.com/spf13/cobra"
)

var mediaCmd = &cobra.Command{
	Use:   "media",
	Short: "Interact with the Micropub media endpoint",
}

func init() {
	rootCmd.AddCommand(mediaCmd)
}
