package main

import (
	"github.com/spf13/cobra"
)

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a post",
}

func init() {
	rootCmd.AddCommand(createCmd)
}
