package main

import (
	"context"
	"net"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.pinniped.dev/pkg/oidcclient/pkce"
	"golang.org/x/oauth2"
	indieauth "hawx.me/code/indieauth/v2"

	"fmt"
	"log"
	"net/http"
	"strings"
)

func webBasedAuthorization(me string) (*oauth2.Token, error) {
	endpoints, err := (&indieauth.Config{}).FindEndpoints(me)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()

	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, err
	}
	port := listener.Addr().(*net.TCPAddr).Port

	config := &oauth2.Config{
		ClientID: "https://micropub-go.tanna.dev/",
		Scopes:   []string{"create update delete undelete media"},
		Endpoint: oauth2.Endpoint{
			AuthURL:   endpoints.Authorization.String(),
			TokenURL:  endpoints.Token.String(),
			AuthStyle: oauth2.AuthStyleInParams,
		},
		RedirectURL: fmt.Sprintf("http://localhost:%d/callback", port),
	}
	pkceVerifier, err := pkce.Generate()
	if err != nil {
		return nil, err
	}

	next := make(chan struct{})

	var tok *oauth2.Token
	state := "some-random-state"

	srv := &http.Server{
		Addr: fmt.Sprintf(":%d", port),
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if state != r.FormValue("state") {
				next <- struct{}{}
			}
			errorCode := r.FormValue("error")
			if len(errorCode) != 0 {
				builder := strings.Builder{}
				builder.WriteString("Received an error on the callback: ")
				builder.WriteString(errorCode)

				if len(r.FormValue("error_description")) != 0 {
					builder.WriteString(", with error_description=")
					builder.WriteString(r.FormValue("error_description"))
				}

				log.Fatal(builder.String())
				next <- struct{}{}
			}
			code := r.FormValue("code")

			tok, err = config.Exchange(ctx, code, pkceVerifier.Verifier())
			if err != nil {
				log.Fatal(err)
			}

			tokenMe := tok.Extra("me").(string)
			newEndpoints, err := (&indieauth.Config{}).FindEndpoints(me)
			if err != nil {
				log.Fatal(err)
				next <- struct{}{}
			}

			if newEndpoints.Authorization.String() != endpoints.Authorization.String() {
				log.Fatalf("The me value provided, %s, has a different authorization endpoint (%s) than the me returned on the token exchange (%s, with authorization endpoint %s)", me, endpoints.Authorization.String(), tokenMe, newEndpoints.Authorization.String())
				next <- struct{}{}
			}

			next <- struct{}{}
		}),
	}

	go func() {
		_ = srv.ListenAndServe()
	}()

	url := config.AuthCodeURL(state, pkceVerifier.Challenge(), pkceVerifier.Method())
	fmt.Println("Visit this URL to authorize the app:", url)

	<-next
	_ = srv.Shutdown(ctx)

	return tok, nil
}

var reauthCmd = &cobra.Command{
	Use:   "reauth",
	Short: "(Re-)authenticate the given profile, fetching a fresh access_token",
	Long: `(Re-)authenticate the given profile, fetching a fresh access_token.

	Performs an authorization code grant.
  `,
	Args: cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}

		token, err := webBasedAuthorization(config.Me)
		if err != nil {
			return nil
		}
		if len(token.AccessToken) != 0 {
			key := configurationPrefix(profile)
			viper.Set(key+".access_token", token.AccessToken)
			viper.Set(key+".me", token.Extra("me").(string))
			err = viper.WriteConfig()
			if err != nil {
				return err
			}
			fmt.Println("Successfully re-authenticated!")
		}
		return nil
	},
}

func init() {
	authCmd.AddCommand(reauthCmd)
}
