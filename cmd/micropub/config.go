package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"www.jvt.me/go/micropub"
)

type config struct {
	AccessToken   string
	Me            string
	Endpoint      string
	MediaEndpoint string
}

func configurationPrefix(profile string) string {
	key := "profiles." + profile
	return key
}

func configForProfile(profile string) (*config, error) {
	key := configurationPrefix(profile)
	c := viper.Sub(key)
	if c == nil {
		return nil, fmt.Errorf("profile `%s` not found", profile)
	}

	return newConfig(c), nil
}

func (config *config) client() *micropub.Client {
	return &micropub.Client{
		AccessToken:   config.AccessToken,
		Endpoint:      config.Endpoint,
		MediaEndpoint: config.MediaEndpoint,
	}
}

func newConfig(v *viper.Viper) *config {
	return &config{
		AccessToken:   v.GetString("access_token"),
		Me:            v.GetString("me"),
		Endpoint:      v.GetString("endpoint"),
		MediaEndpoint: v.GetString("media_endpoint"),
	}
}

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Manage your configuration",
}

func init() {
	rootCmd.AddCommand(configCmd)
}
