package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

var undeleteCmd = &cobra.Command{
	Use:   "undelete {url}",
	Short: "Undelete posts",
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		config, err := configForProfile(profile)
		if err != nil {
			return err
		}
		client := config.client()
		resp, err := client.UndeleteForm(args[0])
		if err != nil {
			return err
		}

		if len(resp.Location) > 0 {
			fmt.Println(resp.Location)
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(undeleteCmd)
}
